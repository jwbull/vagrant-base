# Vagrant Base
A simple Vagrant setup with a collection of Ansible roles to get a devbox up-and-running quickly

## Installation Instructions

### Add git module and create configuration
```
git submodule add https://bitbucket.org/jwbull/vagrant-base.git git_modules/vagrant-base
ln -s git_modules/vagrant-base/Vagrantfile
cp git_modules/vagrant-base/vagrant.yml vagrant.yml
```

### Parse vagrant.yml to get Ansible vault information
```
. git_modules/vagrant-base/bin/parse_yaml.sh
eval $(parse_yaml vagrant.yml 'vagrant_')
```

### Output random password to Ansible vault file
```
eval vagrant_vault_file="${vagrant_vault_path}/${vagrant_host_name}"
openssl rand 32 -base64 -out $vagrant_vault_file
```
