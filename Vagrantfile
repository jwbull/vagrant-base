# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'
settings = YAML.load_file 'vagrant.yml'

VAGRANT_API_VERSION = settings['api_version']
VAGRANT_DISTRO = settings['distro_name']
VAGRANT_HOSTNAME = settings['host_name']
VAGRANT_NETWORK_IP = settings['network_ip']
VAGRANT_ENV = settings['env_name']
VAGRANT_SYNCED_FOLDER = settings['synced_folder']
VAGRANT_PROVISION_FOLDER = settings['provision_folder']
VAGRANT_VAULT_PATH = settings['vault_path']

Vagrant.configure(VAGRANT_API_VERSION) do |config|

  config.vm.box = VAGRANT_DISTRO
  config.vm.hostname = VAGRANT_HOSTNAME
  config.vm.network "forwarded_port", guest: 80, host: 4080 # http
  config.vm.network "forwarded_port", guest: 443, host: 4443 # https
  config.vm.network "forwarded_port", guest: 35729, host: 35729 # livereload
  config.vm.network "private_network", ip: VAGRANT_NETWORK_IP
  config.ssh.forward_agent = true
  config.vm.synced_folder ".", VAGRANT_SYNCED_FOLDER, :owner => 'vagrant', :group => 'vagrant', :mount_options => ['dmode=775', 'fmode=775']

  config.vm.provider "virtualbox" do |v|
    v.name = VAGRANT_HOSTNAME
    v.memory = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4
    v.cpus = `sysctl -n hw.ncpu`.to_i
    v.customize ['modifyvm', :id, '--natdnshostresolver1', 'on']
    v.customize ['modifyvm', :id, '--natdnsproxy1', 'on']
  end

  config.vm.provision "ansible" do |a|
    a.limit = VAGRANT_ENV
    a.vault_password_file = "#{VAGRANT_VAULT_PATH}/#{VAGRANT_HOSTNAME}"
    a.playbook = "#{VAGRANT_PROVISION_FOLDER}/build.yml"
    a.inventory_path = "#{VAGRANT_PROVISION_FOLDER}/hosts"
    a.extra_vars = {
      target: VAGRANT_ENV,
      hostname: VAGRANT_HOSTNAME,
      synced_folder: VAGRANT_SYNCED_FOLDER
    }
  end

end
